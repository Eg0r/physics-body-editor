# Physics Body Editor

Physics Body Editor is originally developed by [Aurelien Ribon](http://www.aurelienribon.com/).

This is the modified version for work with big size pictures.


### Release Notes

- All dependencies updated.
- Support big size pictures.

### Build

> $ gradle init  
> $ gradle jar





   

     
